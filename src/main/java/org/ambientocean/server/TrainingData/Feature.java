package org.ambientocean.server.TrainingData;

import org.ambientocean.server.parser.Parser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Vector;

/**
 * Created by Nirandika Wanigasekara on 27/9/2016.
 */
public class Feature {



    private enum rawUserFeatures {
        Gender, Age, AcceptanceLevel, Network, TechFreak, Activity, Room, InstalledApps
    }

    private enum rawArticleFeatures {
        DevID, IOTType, x, y, Dist, Avail, Version, Network, Brand, Container1Pr, Container2Pr, MaxBrightness, ColorSet
    }

    private double armID;
    private double payoff;
    private Vector<Double> userDetails;
    private Vector<Double> armDetails;
    Parser parser;

    public Feature(String features, double _armID, double _payoff, Parser _parser) {
        armID = _armID;
        payoff = _payoff;
        parser = _parser;
        setUserDetails(features);
        setArmDetails(features);

        System.out.println(userDetails.toString()+ " "+ armDetails.toString()+" "+payoff);
    }

    public double getArmID() {
        return armID;
    }

    public double getPayoff() {
        return payoff;
    }

    private void setUserDetails(String feature) {
        JSONObject jsonObject = new JSONObject(feature);
        JSONObject user = jsonObject.getJSONObject("User");
        userDetails = new Vector<Double>();

        userDetails.add(user.getDouble(rawUserFeatures.Gender.toString()));
        userDetails.add(user.getDouble(rawUserFeatures.Age.toString()));
        userDetails.add(user.getDouble(rawUserFeatures.AcceptanceLevel.toString()));
        parser.parseNetwork(userDetails, user.getString(rawUserFeatures.Network.toString()));
        userDetails.add(user.getDouble(rawUserFeatures.TechFreak.toString()));
        parser.parseActivity(userDetails, user.getString(rawUserFeatures.Activity.toString()));
        userDetails.add(user.getDouble(rawUserFeatures.Room.toString()));
        parser.parseApps(userDetails, user.getJSONArray(rawUserFeatures.InstalledApps.toString()).toString());
    }

    public Vector<Double> getUserDetails() {
        return userDetails;
    }

    public Vector<Double> getArmDetails() {
        return armDetails;
    }

    private void setArmDetails(String feature) {
        JSONObject jsonObject = new JSONObject(feature);
        JSONArray deviceList = jsonObject.getJSONArray("Devices");
        for (Object device : deviceList) {
            JSONObject objDevice = (JSONObject) device;
            double tmpArmID = objDevice.getDouble(rawArticleFeatures.DevID.toString());
            if (IsRecommendedArm(tmpArmID)) {
                setArmDetails(objDevice);
            }

        }
    }

    private void setArmDetails(JSONObject objDevice) {
        armDetails = new Vector<Double>();
        if (objDevice.has(rawArticleFeatures.DevID.toString())) {
            armDetails.add(objDevice.getDouble(rawArticleFeatures.DevID.toString()));
        }
        if (objDevice.has(rawArticleFeatures.IOTType.toString())) {
            parser.parseIotType(armDetails, objDevice.getString(rawArticleFeatures.IOTType.toString()));
        }
        if (objDevice.has(rawArticleFeatures.x.toString())) {
            armDetails.add(objDevice.getDouble(rawArticleFeatures.x.toString()));
        }
        if (objDevice.has(rawArticleFeatures.y.toString())) {
            armDetails.add(objDevice.getDouble(rawArticleFeatures.y.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Dist.toString())) {
            armDetails.add(objDevice.getDouble(rawArticleFeatures.Dist.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Avail.toString())) {
            parser.parseAvailability(armDetails, objDevice.getBoolean(rawArticleFeatures.Avail.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Version.toString())) {
            armDetails.add(objDevice.getDouble(rawArticleFeatures.Version.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Network.toString())) {
            parser.parseNetwork(armDetails, objDevice.getString(rawArticleFeatures.Network.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Brand.toString())) {
            parser.parseApps(armDetails, objDevice.getString(rawArticleFeatures.Brand.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Container1Pr.toString())) {
            armDetails.add(objDevice.getDouble(rawArticleFeatures.Container1Pr.toString()));
        }
        if (objDevice.has(rawArticleFeatures.Container2Pr.toString())){
            armDetails.add(objDevice.getDouble(rawArticleFeatures.Container2Pr.toString()));
        }

        if (objDevice.has(rawArticleFeatures.MaxBrightness.toString())) {
            armDetails.add((double) objDevice.getInt(rawArticleFeatures.MaxBrightness.toString()));
        }
        else{
            parser.handleEmptyFeature(armDetails, 1);
        }
        if (objDevice.has(rawArticleFeatures.ColorSet.toString())) {
            //TODO: Implement how to parse this
        }
    }

    private boolean IsRecommendedArm(double aDouble) {
        return aDouble == armID;
    }

    public String toString(){
        String output = userDetails.toString().replace("[","").replace("]",",");
        output += armDetails.toString().replace("[","").replace("]",",");
        output += payoff;
        return output;
    }


   /* public static void main(String[] args) {
        String test = "{\"Devices\":[{\"DevID\":1.000000,\"IOTType\":\"IOTDEVICE\",\"x\":10.000000,\"y\":5.000000,\"Dist\":10.511898,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"WORKSALWAYS\",\"Container1Pr\":\"0.005034\",\"Container2Pr\":\"0.994966\"},{\"DevID\":5.000000,\"IOTType\":\"IOTDEVICE\",\"x\":5.500000,\"y\":1.000000,\"Dist\":7.500000,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"WORKSALWAYS\",\"Container1Pr\":\"2.0E-6\",\"Container2Pr\":\"0.999998\"},{\"DevID\":4.000000,\"IOTType\":\"IOTDEVICE\",\"x\":5.000000,\"y\":5.000000,\"Dist\":5.522680,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"WORKSALWAYS\",\"Container1Pr\":\"7.0E-6\",\"Container2Pr\":\"0.999993\"},{\"DevID\":3.000000,\"IOTType\":\"IOTDEVICE\",\"x\":10.500000,\"y\":6.000000,\"Dist\":11.011357,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"WORKSALWAYS\",\"Container1Pr\":\"0.003856\",\"Container2Pr\":\"0.996144\"},{\"DevID\":2.000000,\"IOTType\":\"IOTDEVICE\",\"x\":10.500000,\"y\":5.000000,\"Dist\":11.011357,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"WORKSALWAYS\",\"Container1Pr\":\"0.129072\",\"Container2Pr\":\"0.870928\"},{\"DevID\":6.000000,\"IOTType\":\"MONITOR\",\"x\":1.000000,\"y\":5.900000,\"Dist\":1.552418,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"APPLE\",\"Container1Pr\":\"0.033533\",\"Container2Pr\":\"0.966467\"},{\"DevID\":7.000000,\"IOTType\":\"MONITOR\",\"x\":9.900000,\"y\":2.500000,\"Dist\":10.824047,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"APPLE\",\"Container1Pr\":\"0.044596\",\"Container2Pr\":\"0.955404\"},{\"DevID\":10.000000,\"IOTType\":\"MONITOR\",\"x\":10.900000,\"y\":3.200000,\"Dist\":11.629703,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"APPLE\",\"Container1Pr\":\"4.3E-5\",\"Container2Pr\":\"0.999957\"},{\"DevID\":9.000000,\"IOTType\":\"MONITOR\",\"x\":6.000000,\"y\":5.000000,\"Dist\":6.519202,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"APPLE\",\"Container1Pr\":\"0.046355\",\"Container2Pr\":\"0.953645\"},{\"DevID\":8.000000,\"IOTType\":\"MONITOR\",\"x\":3.000000,\"y\":0.150000,\"Dist\":6.393160,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"APPLE\",\"Container1Pr\":\"0.203383\",\"Container2Pr\":\"0.796617\"},{\"DevID\":22.000000,\"IOTType\":\"LIGHTBULB\",\"x\":3.500000,\"y\":2.000000,\"Dist\":5.315073,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"LIFX\",\"Container1Pr\":\"0.999194\",\"Container2Pr\":\"8.06E-4\",\"MaxBrightness\":2,\"ColorSet\":[]},{\"DevID\":25.000000,\"IOTType\":\"LIGHTBULB\",\"x\":4.000000,\"y\":4.000000,\"Dist\":4.743416,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"5.47E-4\",\"Container2Pr\":\"0.999453\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":28.000000,\"IOTType\":\"LIGHTBULB\",\"x\":8.000000,\"y\":4.000000,\"Dist\":8.631338,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.885122\",\"Container2Pr\":\"0.114878\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":32.000000,\"IOTType\":\"LIGHTBULB\",\"x\":9.000000,\"y\":2.500000,\"Dist\":9.962429,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"5.13E-4\",\"Container2Pr\":\"0.999487\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":12.000000,\"IOTType\":\"LIGHTBULB\",\"x\":0.000000,\"y\":4.000000,\"Dist\":1.581139,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.999926\",\"Container2Pr\":\"7.4E-5\",\"MaxBrightness\":6,\"ColorSet\":[]},{\"DevID\":20.000000,\"IOTType\":\"LIGHTBULB\",\"x\":3.000000,\"y\":1.000000,\"Dist\":5.700877,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.010969\",\"Container2Pr\":\"0.989031\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":16.000000,\"IOTType\":\"LIGHTBULB\",\"x\":1.000000,\"y\":5.500000,\"Dist\":1.500000,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.122528\",\"Container2Pr\":\"0.877472\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":33.000000,\"IOTType\":\"LIGHTBULB\",\"x\":9.000000,\"y\":5.500000,\"Dist\":9.500000,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.981337\",\"Container2Pr\":\"0.018663\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":23.000000,\"IOTType\":\"LIGHTBULB\",\"x\":3.500000,\"y\":4.000000,\"Dist\":4.272002,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.002948\",\"Container2Pr\":\"0.997052\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":21.000000,\"IOTType\":\"LIGHTBULB\",\"x\":3.000000,\"y\":5.500000,\"Dist\":3.500000,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"LIFX\",\"Container1Pr\":\"0.961868\",\"Container2Pr\":\"0.038132\",\"MaxBrightness\":4,\"ColorSet\":[]},{\"DevID\":35.000000,\"IOTType\":\"LIGHTBULB\",\"x\":9.500000,\"y\":2.000000,\"Dist\":10.594810,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.021301\",\"Container2Pr\":\"0.978699\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":11.000000,\"IOTType\":\"LIGHTBULB\",\"x\":0.000000,\"y\":2.000000,\"Dist\":3.535534,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.99986\",\"Container2Pr\":\"1.4E-4\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":14.000000,\"IOTType\":\"LIGHTBULB\",\"x\":0.500000,\"y\":4.000000,\"Dist\":1.802776,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.961618\",\"Container2Pr\":\"0.038382\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":26.000000,\"IOTType\":\"LIGHTBULB\",\"x\":6.000000,\"y\":2.500000,\"Dist\":7.158911,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"LIFX\",\"Container1Pr\":\"0.999978\",\"Container2Pr\":\"2.2E-5\",\"MaxBrightness\":5,\"ColorSet\":[]},{\"DevID\":36.000000,\"IOTType\":\"LIGHTBULB\",\"x\":10.000000,\"y\":4.000000,\"Dist\":10.606602,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.002028\",\"Container2Pr\":\"0.997972\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":19.000000,\"IOTType\":\"LIGHTBULB\",\"x\":2.000000,\"y\":3.500000,\"Dist\":3.201562,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.082964\",\"Container2Pr\":\"0.917036\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":24.000000,\"IOTType\":\"LIGHTBULB\",\"x\":4.000000,\"y\":2.000000,\"Dist\":5.700877,\"Avail\":true,\"Version\":1.000000,\"Network\":\"NUS\",\"Brand\":\"LIFX\",\"Container1Pr\":\"0.002221\",\"Container2Pr\":\"0.997779\",\"MaxBrightness\":2,\"ColorSet\":[]},{\"DevID\":17.000000,\"IOTType\":\"LIGHTBULB\",\"x\":2.000000,\"y\":2.500000,\"Dist\":3.905125,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.052013\",\"Container2Pr\":\"0.947987\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":34.000000,\"IOTType\":\"LIGHTBULB\",\"x\":9.000000,\"y\":6.000000,\"Dist\":9.513149,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.987734\",\"Container2Pr\":\"0.012266\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":31.000000,\"IOTType\":\"LIGHTBULB\",\"x\":9.000000,\"y\":2.000000,\"Dist\":10.124228,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.999649\",\"Container2Pr\":\"3.51E-4\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":13.000000,\"IOTType\":\"LIGHTBULB\",\"x\":0.500000,\"y\":2.000000,\"Dist\":3.640055,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.999539\",\"Container2Pr\":\"4.61E-4\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":27.000000,\"IOTType\":\"LIGHTBULB\",\"x\":6.000000,\"y\":3.500000,\"Dist\":6.800735,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.978374\",\"Container2Pr\":\"0.021626\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":18.000000,\"IOTType\":\"LIGHTBULB\",\"x\":2.000000,\"y\":3.000000,\"Dist\":3.535534,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.028371\",\"Container2Pr\":\"0.971629\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":30.000000,\"IOTType\":\"LIGHTBULB\",\"x\":9.000000,\"y\":1.500000,\"Dist\":10.307764,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.990876\",\"Container2Pr\":\"0.009124\",\"MaxBrightness\":1,\"ColorSet\":[]},{\"DevID\":29.000000,\"IOTType\":\"LIGHTBULB\",\"x\":8.500000,\"y\":2.000000,\"Dist\":9.656604,\"Avail\":true,\"Version\":0.500000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.996451\",\"Container2Pr\":\"0.003549\",\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":15.000000,\"IOTType\":\"LIGHTBULB\",\"x\":1.000000,\"y\":1.000000,\"Dist\":4.743416,\"Avail\":true,\"Version\":0.000000,\"Network\":\"NUS\",\"Brand\":\"HUE\",\"Container1Pr\":\"0.037184\",\"Container2Pr\":\"0.962816\",\"MaxBrightness\":5,\"ColorSet\":[]}],\"User\":{\"Gender\":1, \"Age\":18, \"AcceptanceLevel\":0.800000,\"Network\":\"NUS\", \"TechFreak\":1.000000, \"Activity\":\"BREAK\",\"Room\":\"01\",\"InstalledApps\":[\"WORKSALWAYS\"]}}";
        Feature feature = new Feature(test, 6.0, 1.0, new SimulationDataParser());


    }*/


}

