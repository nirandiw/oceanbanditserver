package org.ambientocean.server.TrainingData;


import org.ambientocean.server.parser.SimulationDataParser;
import org.ambientocean.server.utils.Writer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Nirandika Wanigasekara on 26/9/2016.
 */
public class ProcessTrainingData {


    public static boolean processTrainigData(String fileName) {
        try {
            String TRAINING_DATA_READ_FILE_PATH = "D:\\intellij_projects\\OceanBanditServer\\src\\main\\java\\org\\ambientocean\\server\\TrainingData\\";
            BufferedReader br = new BufferedReader(new FileReader(TRAINING_DATA_READ_FILE_PATH +fileName));
            while (true){
                String featureStr = br.readLine();
                String payoff = br.readLine();
                String armId = br.readLine();
                if (featureStr==null||payoff==null||armId==null){
                    break;
                }else{
                    Feature feature = new Feature(featureStr, Double.parseDouble(armId),Double.parseDouble(payoff), new SimulationDataParser());
                    Writer.appendToFile(TRAINING_DATA_READ_FILE_PATH +fileName+".csv",feature.toString(),true );


                }             }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public static void main(String[] args){
        processTrainigData("2016-10-24");
    }



}
