package org.ambientocean.server.controller;

import org.ambientocean.server.parser.SimulationDataParser;
import org.ambientocean.server.utils.CommonMethods;
import org.ambientocean.server.utils.ConstantValues;
import org.oceanbandit.solver.BanditSolver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Nirandika Wanigasekara on 22/4/2016.
 */
@WebServlet(name = "sBestGraphServlet", urlPatterns = "/SimulationBestGraphServlet/sBestGraphServlet")
public class SimulationBestGraphServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(OceanBestGraphServlet.class.getName());
    //BanditSolver banditSolver = new BanditSolver("SIMULATION", ConstantValues.SIMULATION_DATABASE_NAME, ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO, ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES, 14, ConstantValues.SIMULATION_COMMON_FEATURE_DIMENSION, ConstantValues.SIMULATION_TABLE_NAME_USERIFNO);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String intent = request.getParameter(ConstantValues.INTENT);
        String algoVersion = request.getParameter(ConstantValues.ALGO_VERSION);
        System.out.println("Algorithm type "+algoVersion+ " Intent "+intent);
        String feature = request.getParameter(ConstantValues.FEATURE);
        //Writer.appendToFile("D:\\intellij_projects\\OceanBanditServer\\src\\main\\java\\org\\ambientocean\\server\\controller\\SampleInput.txt", feature, true);
        System.out.println("feature received " + feature);
        HashMap<String, Integer> numberOfDevices = CommonMethods.getNumberOfDevices(request);


        SimulationDataParser simulationDataParser = new SimulationDataParser();
        double[] newFeature = simulationDataParser.getDistanceToIoTDevice(feature);
        String bestGraph = "";
        //bestGraph="<id> 2</id><?xml version=\"1.0\" encoding=\"UTF-8\"?><ControlGraph><receiver>org.ambientdynamix.contextplugins.hueplugin</receiver><controller>org.ambientdynamix.contextplugins.ambienttwitternew</controller><controlEdge><commandType>SWITCH</commandType><name>notify</name><sourcePlugin>org.ambientdynamix.contextplugins.ambienttwitternew</sourcePlugin><sourceDeviceId>nirandiw</sourceDeviceId><targetPlugin>org.ambientdynamix.contextplugins.hueplugin</targetPlugin><targetDeviceId>TEST</targetDeviceId></controlEdge></ControlGraph>";

        //banditSolver.preProcess(); //TODO will not be necessary for all the instance. So think of how to remove it.
        log.log(Level.FINE, "OBGBG preProcessing done");

        if (algoVersion.equals("1")) { //LinUCB-Disjoint
            BanditSolver banditSolver1 = new BanditSolver(intent, ConstantValues.SIMULATION_DATABASE_NAME,
                    ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO,
                    ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES, newFeature.length,
                    ConstantValues.ALPHA_SERVER_SIDE, numberOfDevices);
            bestGraph = banditSolver1.runLinUCBDisjointModel(newFeature, false);
        } else if (algoVersion.equals("2")) { //LinUCB-Hybrid
            double[] newUser = simulationDataParser.getUserProfile(feature);
            BanditSolver banditSolver2 = new BanditSolver(intent, ConstantValues.SIMULATION_DATABASE_NAME,
                    ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO,
                    ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES, newFeature.length,
                    ConstantValues.SIMULATION_COMMON_FEATURE_DIMENSION, ConstantValues.SIMULATION_TABLE_NAME_USERIFNO,
                    ConstantValues.ALPHA_SERVER_SIDE,numberOfDevices);
            try {
                //String filterCommand = getFilteringSQLCondition(intent);
                bestGraph = banditSolver2.runLinUCBHybridModel(newFeature,newUser, false, "","", intent);
                System.out.println("Ocean Server best graph"+bestGraph);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            bestGraph =" WARNNING!! Invalid algorithm version";
        }


        System.out.println("Server side best graph= " + bestGraph);
        /*if (Double.parseDouble(bestGraph) <= 0.0){
            System.exit(-1);
            System.out.println("Invalid Graph Selected");
        }*/
        response.getWriter().write(bestGraph);


    }




    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write("Ocean Bandit servlet hello!!");
    }


}
