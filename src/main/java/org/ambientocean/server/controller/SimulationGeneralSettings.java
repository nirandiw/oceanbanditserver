package org.ambientocean.server.controller;

import org.ambientocean.server.utils.ConstantValues;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by Nirandika Wanigasekara on 26/4/2016.
 */
@WebServlet(name = "sGeneralServlet", urlPatterns = "/SimulationGeneralServlet/sGeneralServlet")
public class SimulationGeneralSettings extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = null;
        try {
            connection = getDBConnection();
            String sql_delete_contextinfo = "DELETE FROM `contextinfo` WHERE 1 ";
            String sql_update_configgraphs = "UPDATE configgraphs SET example_count=0,feature_set='',`is_new`=1,response_set='' ,user_arm_matrix='' WHERE 1";
            String sql_delete_userinfo = "DELETE FROM `userinfo` WHERE 1 ";
            String sql_update_commoninfo = "UPDATE `commoninfo` SET `common_feature_mat`='',`common_response_mat`='',`beta`='' WHERE `id`=1";
            Statement statementFeatureSet;
            int featureSet;
            statementFeatureSet = connection.createStatement();
            featureSet = statementFeatureSet.executeUpdate(sql_delete_contextinfo);
            System.out.println("DATABASE DELETE CONTEXT_INFO "+featureSet);
            featureSet = statementFeatureSet.executeUpdate(sql_update_configgraphs);
            System.out.println("DATABASE RESET CONFIGUR_GRAPHS "+featureSet);
            featureSet = statementFeatureSet.executeUpdate(sql_delete_userinfo);
            System.out.println("DATABASE DELETE USER_INFO "+featureSet);
            featureSet = statementFeatureSet.executeUpdate(sql_update_commoninfo);
            System.out.println("DATABASE RESET COMMON_INFO "+featureSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private Connection getDBConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(org.oceanbandit.constantvalues.ConstantValues.DATABASE_HOST_IP + ConstantValues.SIMULATION_DATABASE_NAME, "ocean", "mit123");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
