package org.ambientocean.server.controller;

import org.ambientdynamix.contextplugins.ocean.InteractionReport;
import org.ambientocean.server.utils.ConstantValues;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * Created by Nirandika Wanigasekara on 13/11/2015.
 */
@WebServlet(name = "oContextSnapshotsServlet", urlPatterns = "/OceanContextSnapshotsServlet/oContextSnapshotsServlet")
public class OceanContextSnapshotsServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(OceanContextSnapshotsServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(req.getInputStream());
            InteractionReport interactionReport = (InteractionReport) objectInputStream.readObject();
            //System.out.println(interactionReport.getContextSnapshots());
            saveInteractionReport(interactionReport); //TODO: Handle errors in the response

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean saveInteractionReport(InteractionReport interactionReport) {
        boolean isSaveSuccessful= false;
        ObjectMapper objectMapper= new ObjectMapper();
        try {
            String serializedInteractionReport= objectMapper.writeValueAsString(interactionReport);

            Connection connection;
            connection = DriverManager.getConnection(ConstantValues.HOST_DATABASE_ADDRESS + ConstantValues.DATABASE_NAME, "ocean", "mit123");

            Statement statement = connection.createStatement();
            StringBuffer insertSQL = new StringBuffer("INSERT INTO " + ConstantValues.TABLE_NAME_INTERACTION_REPORTS +  "(interaction_report ) VALUES ('");
            insertSQL.append(serializedInteractionReport+"')");
            System.out.println(insertSQL);
            int resultSet = statement.executeUpdate(insertSQL.toString());//TODO: Check out the return value and handle exceptions.
            if (resultSet!=-1){
                isSaveSuccessful=true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isSaveSuccessful;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write("Ocean Context Snapshot servlet hello!!");
    }
}
