package org.ambientocean.server.controller;

import org.ambientocean.server.parser.SimulationDataParser;
import org.ambientocean.server.utils.CommonMethods;
import org.ambientocean.server.utils.ConstantValues;
import org.ambientocean.server.utils.Writer;
import org.oceanbandit.solver.BanditSolver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Nirandika Wanigasekara on 22/4/2016.
 */


@WebServlet(name = "sFeedbackServlet", urlPatterns = "/SimulationFeedbackServlet/sFeedbackServlet")
public class SimulationFeedbackServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(OceanFeedbackServlet.class.getName());
    double numberOfClicks = 0;
    double numberOfIterations = 0;
    private static String TRAINING_DATA_PATH = "D:\\intellij_projects\\OceanBanditServer\\src\\main\\java\\org\\ambientocean\\server\\TrainingData\\";
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.log(Level.INFO,"Simulation Feedback");

        String intent = request.getParameter(ConstantValues.INTENT);
        System.out.println("intent "+intent);
        String feature = request.getParameter(ConstantValues.FEATURE);
        System.out.println("features "+feature);
        String strPayOffs = request.getParameter(ConstantValues.FEEDBACK);

        HashMap<Double, Double> payOffs = processPayoffs(strPayOffs);
        System.out.println("Feedback "+payOffs);

        HashMap<String, Integer> numberOfDevices = CommonMethods.getNumberOfDevices(request);

        logger.log(Level.INFO,"intent " + intent + "feature " + feature + "feedback " + payOffs);


       //String feedbackForGraphId = request.getParameter(ConstantValues.FEEDBACK_FOR_GRAPH);

        SimulationDataParser simulationDataParser = new SimulationDataParser();

        double[] newFeature = simulationDataParser.getDistanceToIoTDevice(feature);
        System.out.println("feedback feature size " + newFeature.length);

        String algoVersion = request.getParameter(ConstantValues.ALGO_VERSION);

        double firstChoice =0.0;
        if (request.getParameter(ConstantValues.FIRST_CHOICE)!=null) {
            firstChoice = Double.parseDouble(request.getParameter(ConstantValues.FIRST_CHOICE));
        }

        if (algoVersion.equals("1")) {
            System.out.println("algo version 1");
            BanditSolver banditSolver = new BanditSolver(intent, ConstantValues.SIMULATION_DATABASE_NAME,
                    ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO,
                    ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES, newFeature.length,
                    ConstantValues.ALPHA_SERVER_SIDE, numberOfDevices);

            //writeNewFeatureToDataBase(realValuedPayOff, true, inputRecordArm, maxPayOffArmID, connection);
            //arm =
            banditSolver.writeNewFeatureToDataBaseDisjointModelServerCall(payOffs, false, newFeature); //TODO: Change this to the updated code
        } else if (algoVersion.equals("2")) {
            System.out.println("algo 2");
            double[] newUser = simulationDataParser.getUserProfile(feature);
            writeTrainingData(feature, payOffs, true);
            BanditSolver banditSolver = new BanditSolver(intent, ConstantValues.SIMULATION_DATABASE_NAME,
                    ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO,
                    ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES, newFeature.length,
                    ConstantValues.SIMULATION_COMMON_FEATURE_DIMENSION, ConstantValues.SIMULATION_TABLE_NAME_USERIFNO,
                    ConstantValues.ALPHA_SERVER_SIDE, numberOfDevices);

            banditSolver.writeNewFeatureToDataBaseHybridModelServerCall(payOffs, false, newFeature, newUser);


        } else {
            System.out.println(" WARNNING!! Invalid algorithm version");
        }

        System.out.println("Number of clicks = " + numberOfClicks);
        double ctr = numberOfClicks / (numberOfIterations);
        System.out.println("Numher of iterations= " + (numberOfIterations));
        System.out.println("CTR =" + ctr);


    }

    private HashMap<Double, Double> processPayoffs(String strPayOffs) {
        HashMap<Double, Double> p = new HashMap<Double, Double>();
        for (String s: strPayOffs.split(";")){
            String[] values = s.split(":");
            p.put(Double.parseDouble(values[0]), Double.parseDouble(values[1]));
            if (Double.parseDouble(values[1]) == 1) {
                numberOfClicks += 1;
            }
            numberOfIterations += 1;
        }
        return p;
    }

    private void writeTrainingData(String feature, HashMap<Double, Double> payOffs, Boolean isTraining) {
        if (isTraining) {
            Date currentDate = Calendar.getInstance().getTime();
            for (Double arm: payOffs.keySet()){
                Writer.appendToFile(TRAINING_DATA_PATH + dateFormat.format(currentDate), feature, false);
                Writer.appendToFile(TRAINING_DATA_PATH + dateFormat.format(currentDate), payOffs.get(arm).toString(), false);
                Writer.appendToFile(TRAINING_DATA_PATH + dateFormat.format(currentDate), arm.toString(), true);
            }

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
