package org.ambientocean.server.controller;

import org.ambientocean.server.utils.CommonMethods;
import org.ambientocean.server.utils.ConstantValues;
import org.oceanbandit.solver.BanditSolver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Nirandika Wanigasekara on 7/4/2015.
 */
@WebServlet(name = "oFeedbackServlet", urlPatterns = "/OceanFeedbackServlet/oFeedbackServlet")
public class OceanFeedbackServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(OceanFeedbackServlet.class.getName());
    double numberOfClicks = 0;
    double numberOfIterations = 0;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String intent = request.getParameter(ConstantValues.INTENT);
        String feature = request.getParameter(ConstantValues.FEATURE);
        String payOff = request.getParameter(ConstantValues.FEEDBACK);
        HashMap<String, Integer> numberOfDevices = CommonMethods.getNumberOfDevices(request);
        numberOfIterations += 1;
        if (Double.parseDouble(payOff) == 1) {
            numberOfClicks += 1;
        }
        String feedbackForGraphId = request.getParameter(ConstantValues.FEEDBACK_FOR_GRAPH);

        System.out.println("feature received " + feature);
        String[] featureArray = feature.replace("]", "").replace("[", "").split(", ");
        double[] newFeature = CommonMethods.getDoubleArrayFromFeatureArray(featureArray);
        System.out.println("new feature size" + newFeature.length);

        BanditSolver banditSolver = new BanditSolver(intent, ConstantValues.DATABASE_NAME,
                ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO,
                ConstantValues.MAX_NUMBER_OF_FEATURES, newFeature.length, ConstantValues.ALPHA_SERVER_SIDE, numberOfDevices);


        boolean isSuccessful = false;//banditSolver.writeNewFeatureToDataBase(Double.parseDouble(payOff), false, newFeature, Double.parseDouble(feedbackForGraphId)); //TODO: Change this to the updated code
        String feedBack = isSuccessful ? "SUCCESSFUL" : "NOT_SUCCESSFUL";
        log.log(Level.FINE, feedBack);
        response.getWriter().write(feedBack);
        System.out.println("Number of clicks = " + numberOfClicks);
        double ctr = numberOfClicks / numberOfIterations;
        System.out.println("Numher of iterations= " + numberOfIterations);
        System.out.println("CTR =" + ctr);
        File file = new File("LinUCB_CTR.txt");
        FileWriter writer = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write(numberOfIterations + "," + numberOfClicks + "," + ctr);
        bw.close();

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write("Ocean Bandit servlet hello!!");
    }


}
