package org.ambientocean.server.controller;

import org.ambientocean.server.utils.CommonMethods;
import org.ambientocean.server.utils.ConstantValues;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Nirandika Wanigasekara on 3/4/2015.
 */
@WebServlet(name="oBestGraphServlet", urlPatterns = "/OceanBestGraphServlet/oBestGraphServlet")
public class OceanBestGraphServlet extends HttpServlet{
    private static final Logger log = Logger.getLogger( OceanBestGraphServlet.class.getName() );


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{


        String intent=request.getParameter(ConstantValues.INTENT);

        String feature=request.getParameter(ConstantValues.FEATURE);
        System.out.println("feature received "+feature);

        String[] featureArray= feature.replace("]","").replace("[","").split(", ");
        double[] newFeature= CommonMethods.getDoubleArrayFromFeatureArray(featureArray);


       // BanditSolver banditSolver = new BanditSolver(intent, ConstantValues.DATABASE_NAME,
        //        ConstantValues.TABLE_NAME_CONFIGGRAPH, ConstantValues.TABLE_NAME_CONTEXTINFO, ConstantValues.MAX_NUMBER_OF_FEATURES, newFeature.length);

      //  banditSolver.preProcess(); //TODO will not be necessary for all the instance. So think of how to remove it.
        log.log(Level.FINE, "OBGBG preProcessing done");

        String bestGraph = "1";//banditSolver.runLinUCB(newFeature, false);
        //bestGraph="<id> 2</id><?xml version=\"1.0\" encoding=\"UTF-8\"?><ControlGraph><receiver>org.ambientdynamix.contextplugins.hueplugin</receiver><controller>org.ambientdynamix.contextplugins.ambienttwitternew</controller><controlEdge><commandType>SWITCH</commandType><name>notify</name><sourcePlugin>org.ambientdynamix.contextplugins.ambienttwitternew</sourcePlugin><sourceDeviceId>nirandiw</sourceDeviceId><targetPlugin>org.ambientdynamix.contextplugins.hueplugin</targetPlugin><targetDeviceId>TEST</targetDeviceId></controlEdge></ControlGraph>";
        System.out.println("Server side best graph= "+bestGraph);

        response.getWriter().write(bestGraph);


    }
    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
        response.getWriter().write("Ocean Bandit servlet hello!!");
    }



}
