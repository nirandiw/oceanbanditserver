package org.ambientocean.server.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Nirandika Wanigasekara on 19/2/2017.
 */


@WebServlet(name = "sTopsisServlet", urlPatterns = "/SimulationTopsisServlet/sTopsisServlet")
public class SimulationTopsisServlet extends HttpServlet {
    public static int step =1;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //int step =Integer.valueOf(request.getParameter("STEP"));
        BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\intellij_projects\\OceanBanditServer\\src\\main\\java\\org\\ambientocean\\server\\controller\\topsis_output.txt"));
        int counter =0;
        String line = null;
        while (counter!=step){
            line = bufferedReader.readLine();
            counter++;
        }
        System.out.println(step+" counter: "+counter+ "line "+line);
        step++;

        response.getWriter().write(line);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
