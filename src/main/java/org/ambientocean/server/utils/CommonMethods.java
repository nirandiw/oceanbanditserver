package org.ambientocean.server.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by Nirandika Wanigasekara on 7/4/2015.
 */
public class CommonMethods {

    public static double[] getDoubleArrayFromFeatureArray(String[] featureArray) {
        double[] newFeature= new double[featureArray.length];
        for (int i=0;i<featureArray.length;i++){
            newFeature[i]=Double.parseDouble(featureArray[i]); //TODO:Handle the number format exception.May be add noisy data
        }
        return newFeature;
    }

    public static HashMap<String, Integer> getNumberOfDevices(HttpServletRequest request) {
        HashMap<String, Integer> numberOfDevices= new HashMap<String, Integer>();
        if (request.getParameter(ConstantValues.NUMBER_OF_DEVICES)!=null){
            String devices = request.getParameter(ConstantValues.NUMBER_OF_DEVICES);
            String[] combinationOfDevices = devices.split(";");
            for (String s : combinationOfDevices){
                String[] details = s.split(":");
                numberOfDevices.put(details[0],Integer.parseInt(details[1]));
            }
        }
        return numberOfDevices;
    }
}
