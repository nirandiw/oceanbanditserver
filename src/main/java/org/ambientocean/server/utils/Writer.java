package org.ambientocean.server.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.StringJoiner;

/**
 * Created by Nirandika Wanigasekara on 26/9/2016.
 */
public class Writer {
    public static void appendToFile(String fileName, String thingToAppend, boolean includeNewLine){
        try{
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(new File(fileName),true));
            printWriter.append(thingToAppend);
            if (includeNewLine){
                printWriter.append("\n");
            }else {
                printWriter.append(",");
            }
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void appendToCSVFile(String fileName, String[] stringToAppend){
        StringJoiner stringJoiner = new StringJoiner(",");
        for (String value: stringToAppend){
            stringJoiner.add(value);
        }
        appendToFile(fileName, stringJoiner.toString(), true);
    }

}
