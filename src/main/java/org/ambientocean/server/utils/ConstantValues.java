package org.ambientocean.server.utils;

/**
 * Created by Nirandika Wanigasekara on 4/4/2015.
 */
public class ConstantValues {
    //Parameter name
    public static final String INTENT= "INTENT";
    public static final String FEATURE="FEATURE";
    public static final String FEEDBACK= "FEEDBACK";
    public static final String FEEDBACK_FOR_GRAPH="FEEDBACK_FOR_GRAPH";

    //BandiSolver constructor values
    public static final String DATABASE_NAME= "ocean";
    public static final String SIMULATION_DATABASE_NAME = "simulation";
    public static final String TABLE_NAME_CONFIGGRAPH= "configgraphs";
    public static final String TABLE_NAME_CONTEXTINFO= "contextinfo";
    public static final String TABLE_NAME_INTERACTION_REPORTS= "interaction_reports";
    public static final int MAX_NUMBER_OF_FEATURES=4;
    public static final int SIMULATION_MAX_NUMBER_OF_FEATURES =13;

    public static final String HOST_DATABASE_ADDRESS ="jdbc:mysql://172.26.191.229:3306/";


    public static final String ALGO_VERSION = "ALGO_VERSION" ;
    public static final int SIMULATION_COMMON_FEATURE_DIMENSION = 8;
    public static final String SIMULATION_TABLE_NAME_USERIFNO = "userinfo";
    public static final String NUMBER_OF_DEVICES = "NUMBER_OF_DEVICES";
    public static final String FIRST_CHOICE ="FIRST_CHOICE";

    public static double ALPHA_SERVER_SIDE =1; //0.6 was good.

}
