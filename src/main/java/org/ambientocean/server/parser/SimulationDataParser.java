package org.ambientocean.server.parser;

/**
 * Created by Nirandika Wanigasekara on 22/4/2016.
 */

import org.ambientocean.server.utils.ConstantValues;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SimulationDataParser implements Parser{

    private enum IOTTypes {
        IOTDEVICE, LIGHTBULB, MONITOR
    }

    public enum Brand {
        HUE, LIFX, APPLE, WORKSALWAYS
    }



    private enum Network {
        NUS, LAB01, LAB02
    }

    public enum Activity {
        PRESENTATION, CONVERSATION, BREAK, BRAINSTORMING, READING, VISUALLY_DEMANDING
    }

    private static final Logger LOGGER = Logger.getLogger(SimulationDataParser.class.getName());


    public double[] getDistanceToIoTDevice(String jsonString) {

        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray deviceList = jsonObject.getJSONArray("Devices");
        int numberOfDevice = ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES;//deviceList.length();
        double[] distanceToIoTDevicesArray = new double[numberOfDevice];
        int counter=0;
        System.out.println(" Context Feature \n");
        for ( Object device : deviceList) {
            if (counter==ConstantValues.SIMULATION_MAX_NUMBER_OF_FEATURES){
                break;
            }
               JSONObject jsonDevice=(JSONObject)device;
                double distance =jsonDevice.getDouble("Dist");
                distanceToIoTDevicesArray[counter]=distance;
            System.out.print(distance+",");
            counter++;
        }
        /*
        JSONObject user= jsonObject.getJSONObject("User");
        String userRoom = user.getString("Room");
        if (userRoom.equals("1")){
            distanceToIoTDevicesArray[counter]=1;
            //counter++;
            //distanceToIoTDevicesArray[counter]=0;
        }else{
            distanceToIoTDevicesArray[counter]=0;
            //counter++;
            //distanceToIoTDevicesArray[counter]=1;
        }
        */
        System.out.println("\n");
        return distanceToIoTDevicesArray;
    }

    public double[] getUserProfile(String jsonString) {
        String[] userFeaturesOrder ={"Gender","Age","AcceptanceLevel","Network", "TechFreak", "Activity","InstalledApps"}; //TODO Think of how to impose order in array iteration
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONObject user = jsonObject.getJSONObject("User");
        int numberOfUserFeature= user.keySet().size();
        ArrayList<Double> userFeature= new ArrayList<Double>();
        int counter=0;
        System.out.println(" User Feature \n");
        double val= Double.NaN;
        for (String key: user.keySet()){
            if (key.equals("InstalledApps")){
                JSONArray installedApps=user.getJSONArray(key);
                boolean lifx=false;
                boolean hue= false;
                for ( Object app : installedApps){
                    if (app.toString().equals("LIFX")){
                        lifx=true;
                    }
                    if (app.toString().equals("HUE")){
                        hue=true;
                    }
                }
                val= lifx?1:0;
                System.out.print(val+" LIF,");
                userFeature.add(val);
                counter++;
                val= hue? 1:0;
                userFeature.add(val);
                System.out.print(val+"HUE,");
                counter++;
            }else if(key.equals("Network") || key.equals("Activity")){
                val = parseSimulationUserInformation(key, user.getString(key));
                userFeature.add(val);
                System.out.print(val+"W,");
                counter++;
            }else if(key.equals("Room")) {
                   //Do nothing . There is an issue here.
            }else{
                val = user.getDouble(key);
                userFeature.add(val);
                System.out.print(val+",");
                counter++;
            }

        }
        System.out.println("\n");
        double[] userFeatureDoubleArray = new double[userFeature.size()];
        int i=0;
        for (Double v: userFeature){
            userFeatureDoubleArray[i]=v;
            i++;
        }
        return userFeatureDoubleArray;
    }

    public double parseSimulationUserInformation(String key, String input){
        double value = Double.NaN;
        try {

            value = Double.parseDouble(input);
            return value;
        }catch(NumberFormatException ex){
            if (key.equals("Network")){
                return networkTmp.valueOf(input).value;
            }if (key.equals("Activity")){
                return activityTmp.valueOf(input).value;
            }
        }
        return value;

    }

    //Contains the networks we have in the simulation. We map it to a numerical value here.
    private enum networkTmp {
        NUS(0),  LAB01(1), LAB02(2);
        private double value;

        private networkTmp(double value) {
            this.value = value;
        }
    }

    private enum activityTmp {
        PRESENTATION(0), CONVERSATION(1), BREAK(2), BRAINSTORMING(3), READING(4), VISUALLY_DEMANDING(5);
        private double value;

        private activityTmp(double value) {
            this.value = value;
        }
    }

    public boolean parseAvailability(Vector<Double> armDetails, boolean isAvail) {
        //TODO Null unhandled
        if (isAvail) {
            armDetails.add(1.0);
        } else {
            armDetails.add(0.0);
        }
        return true;
    }

    public boolean parseIotType(Vector<Double> armDetails, String iotType) {
        if (iotType == null) {
            return handleEmptyFeature(armDetails, 3);
        }
        if (iotType.equals(IOTTypes.IOTDEVICE.toString())) {
            armDetails.add(1.0);
            armDetails.add(0.0);
            armDetails.add(0.0);
        } else if (iotType.equals(IOTTypes.LIGHTBULB.toString())) {
            armDetails.add(0.0);
            armDetails.add(1.0);
            armDetails.add(0.0);
        } else if (iotType.equals(IOTTypes.MONITOR.toString())) {
            armDetails.add(0.0);
            armDetails.add(0.0);
            armDetails.add(1.0);
        } else {
            LOGGER.log(Level.WARNING, "Unhandled IoT Type");
            return handleEmptyFeature(armDetails, 3);

        }
        return true;
    }

    public boolean parseNetwork(Vector<Double> detailContainer, String _network) {
        if (_network == null) {
            return handleEmptyFeature(detailContainer, 3);
        }
        if (Network.NUS.toString().equals(_network)) {
            detailContainer.add(1.0);
            detailContainer.add(0.0);
            detailContainer.add(0.0);
        } else if (Network.LAB01.toString().equals(_network)) {
            detailContainer.add(0.0);
            detailContainer.add(1.0);
            detailContainer.add(0.0);
        } else if (Network.LAB02.toString().equals(_network)) {
            detailContainer.add(0.0);
            detailContainer.add(1.0);
            detailContainer.add(0.0);
        } else {
            LOGGER.log(Level.WARNING, "Unhandled or Empty Network Details : " + _network);
            return handleEmptyFeature(detailContainer, 3);
        }
        return true;
    }

    //TODO: Assumes only one kind of app. This needs to change
    public boolean parseApps(Vector<Double> _userDetails, String installedApps) {
        if (installedApps == null) {
            return handleEmptyFeature(_userDetails, 4);
        }
        handleEmptyFeature(_userDetails, 4);
        if (installedApps.contains(Brand.APPLE.toString())) {
            _userDetails.add(1.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
        } else if (installedApps.contains(Brand.HUE.toString())) {
            _userDetails.add(0.0);
            _userDetails.add(1.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
        } else if (installedApps.contains(Brand.LIFX.toString())) {
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(1.0);
            _userDetails.add(0.0);
        } else if (installedApps.contains(Brand.WORKSALWAYS.toString())) {
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(1.0);
        } else {
            LOGGER.log(Level.WARNING, "Unhandled or Empty Network Details : " + installedApps);
            return handleEmptyFeature(_userDetails, 4);
        }
        return true;
    }

    public boolean handleEmptyFeature(Vector<Double> detailContainer, int dummyVariableCount) {
        for (int i = 0; i < dummyVariableCount; i++) {
            detailContainer.add(0.0);
        }
        return false;
    }

    public boolean parseActivity(Vector<Double> _userDetails, String _activity) {

        if (_activity == null) {
            return handleEmptyFeature(_userDetails, 6);
        }
        if (Activity.BRAINSTORMING.toString().equals(_activity)) {
            _userDetails.add(1.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
        } else if (Activity.BREAK.toString().equals(_activity)) {
            _userDetails.add(0.0);
            _userDetails.add(1.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
        } else if (Activity.CONVERSATION.toString().equals(_activity)) {
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(1.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
        } else if (Activity.PRESENTATION.toString().equals(_activity)) {
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(1.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
        } else if (Activity.READING.toString().equals(_activity)) {
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(1.0);
            _userDetails.add(0.0);
        } else if (Activity.VISUALLY_DEMANDING.toString().equals(_activity)) {
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(0.0);
            _userDetails.add(1.0);
        } else {
            LOGGER.log(Level.WARNING, "Unhandled or Empty Activity Details : " + _activity);
            return handleEmptyFeature(_userDetails, 6);
        }
        return true;
    }




    public static void main(String[] args){
        SimulationDataParser simulationDataParser= new SimulationDataParser();
        //double value=simulationDataParser.parseSimulationUserInformation("Network", "LAB01");
        //System.out.println(value);

        simulationDataParser.getUserProfile("{\"Devices\":[{\"DevID\":1.000000,\"IOTType\":IOTDEVICE,\"x\":10.000000,\"y\":5.000000,\"Dist\":7.648529,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":HUE},{\"DevID\":6.000000,\"IOTType\":MONITOR,\"x\":10.900000,\"y\":3.200000,\"Dist\":8.405354,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":APPLE},{\"DevID\":2.000000,\"IOTType\":MONITOR,\"x\":1.000000,\"y\":5.900000,\"Dist\":2.830194,\"Avail\":true,\"Version\":0.000000,\"Network\":NUS,\"Brand\":APPLE},{\"DevID\":4.000000,\"IOTType\":MONITOR,\"x\":3.000000,\"y\":0.150000,\"Dist\":3.387108,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":APPLE},{\"DevID\":5.000000,\"IOTType\":MONITOR,\"x\":6.000000,\"y\":5.000000,\"Dist\":3.807887,\"Avail\":true,\"Version\":0.000000,\"Network\":NUS,\"Brand\":APPLE},{\"DevID\":3.000000,\"IOTType\":MONITOR,\"x\":9.900000,\"y\":2.500000,\"Dist\":7.467261,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":APPLE},{\"DevID\":11.000000,\"IOTType\":LIGHTBULB,\"x\":3.000000,\"y\":5.500000,\"Dist\":2.061553,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":LIFX,\"MaxBrightness\":4,\"ColorSet\":[]},{\"DevID\":12.000000,\"IOTType\":LIGHTBULB,\"x\":3.000000,\"y\":2.000000,\"Dist\":1.581139,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":LIFX,\"MaxBrightness\":2,\"ColorSet\":[]},{\"DevID\":7.000000,\"IOTType\":LIGHTBULB,\"x\":6.000000,\"y\":2.500000,\"Dist\":3.640055,\"Avail\":true,\"Version\":1.000000,\"Network\":NUS,\"Brand\":LIFX,\"MaxBrightness\":5,\"ColorSet\":[]},{\"DevID\":9.000000,\"IOTType\":LIGHTBULB,\"x\":0.000000,\"y\":4.000000,\"Dist\":2.549510,\"Avail\":true,\"Version\":0.000000,\"Network\":NUS,\"Brand\":HUE,\"MaxBrightness\":6,\"ColorSet\":[]},{\"DevID\":13.000000,\"IOTType\":LIGHTBULB,\"x\":4.000000,\"y\":4.000000,\"Dist\":1.581139,\"Avail\":true,\"Version\":0.500000,\"Network\":NUS,\"Brand\":HUE,\"MaxBrightness\":3,\"ColorSet\":[]},{\"DevID\":10.000000,\"IOTType\":LIGHTBULB,\"x\":-0.500000,\"y\":1.500000,\"Dist\":3.605551,\"Avail\":true,\"Version\":0.000000,\"Network\":NUS,\"Brand\":HUE,\"MaxBrightness\":5,\"ColorSet\":[]},{\"DevID\":14.000000,\"IOTType\":LIGHTBULB,\"x\":9.000000,\"y\":6.000000,\"Dist\":6.964194,\"Avail\":true,\"Version\":0.000000,\"Network\":NUS,\"Brand\":HUE,\"MaxBrightness\":1,\"ColorSet\":[YELLOW,BLUE,GREEN,PINK]},{\"DevID\":8.000000,\"IOTType\":LIGHTBULB,\"x\":8.000000,\"y\":1.200000,\"Dist\":5.961544,\"Avail\":true,\"Version\":0.500000,\"Network\":NUS,\"Brand\":HUE,\"MaxBrightness\":1,\"ColorSet\":[]}],\"User\":{\"Gender\":0, \"Age\":18, \"AcceptanceLevel\":0.600000,\"Network\":NUS, \"TechFreak\":0.000000, \"Activity\":BREAK,\"InstalledApps\":[LIFX]}}");
    }

}
