package org.ambientocean.server.parser;

import java.util.Vector;

/**
 * Created by Nirandika Wanigasekara on 7/10/2016.
 */
public interface Parser {

    boolean parseNetwork(Vector<Double> detailContainer, String _network);
    boolean parseAvailability(Vector<Double> armDetails, boolean isAvail);
    boolean parseIotType(Vector<Double> armDetails, String iotType);
    boolean parseApps(Vector<Double> _userDetails, String installedApps);
    boolean parseActivity(Vector<Double> _userDetails, String _activity);
    boolean handleEmptyFeature(Vector<Double> detailContainer, int dummyVariableCount);
}
